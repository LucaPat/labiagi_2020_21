PACKAGES_18_04="libeigen3-dev \
 libsuitesparse-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libreadline-dev \
 qtdeclarative5-dev \
 qt5-qmake \
 libqglviewer-dev-qt5 \
 libudev-dev \
 freeglut3-dev \
 libgtest-dev \
 arduino \
 arduino-mk \
 libglfw3-dev \
 python-catkin-tools"

sudo apt install ${PACKAGES_18_04}
