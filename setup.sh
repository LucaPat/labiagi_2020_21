#!/bin/bash
CURRENT_DIR=${PWD}
source install_deb_packages.sh
source /opt/ros/melodic/setup.bash
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
sudo apt remove ros-melodic-navigation # we override this
sudo apt install python-rosdep
sudo rosdep init
rosdep update
WS_DIR=${PWD}/workspaces/srrg2_labiagi/
cd ${WS_DIR}/src
catkin_init_workspace
cd ${WS_DIR}
catkin build
cd ${CURRENT_DIR}
cp dl.conf ${HOME}

