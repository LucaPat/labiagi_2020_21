#pragma once
#include <srrg_property/property_eigen.h>
#include "navigation_2d_base.h"
#include <srrg_pcl/point_unprojector_types.h>

namespace srrg2_navigation_2d {
  using namespace srrg2_core;

  class Planner2D: public Navigation2DBase {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    using PropertyCostType = Property_<Matrix_<float>>; // float matrix
    using Vector3fVector   = std::vector<Vector3f, Eigen::aligned_allocator<Vector3f> >; // 
    PARAM(PropertyFloat, range_min, "range_min [meters]", 0.0, nullptr);

    PARAM(PropertyFloat, range_max, "range_max [meters]", 1000.0, nullptr);

    PARAM(PropertyString,
          base_link_frame_id,
          "frame id of the robot origin",
          "base_link",
          nullptr);

    PARAM(PropertyString,
          odom_topic,
          "topic where to read the odometry",
          "/odom",
          nullptr);

    PARAM(PropertyVector_<float>,
          cost_coeffs,
          "cost = p0 * distance_to_neighbor + sum_i p_i / (dist-robot_radius)",
          std::vector<float>(),
          &_cost_map_changed_flag);


    PARAM(PropertyString,
          goal_pose_topic,
          "move_base_simple/goal",
          "",
          nullptr);

      PARAM(PropertyFloat,
          scan_voxelize_resolution,
          "subsamples the laser endpoints for efficiency, 0: disables",
          0.05,
          nullptr);

    
    Planner2D();

    // shell commands with response
    bool cmdSetGoal(std::string& response, float x, float y, float theta);
    
    void setMap(GridMap2DPtr grid_map) override;

  protected:
    bool handleSetGoal(BaseSensorMessagePtr msg_);
    bool handleScan(BaseSensorMessagePtr msg_);
    bool computePolicy(const Vector3f& goal) const;
    void updateCostLayers() const;
    bool putMessage(BaseSensorMessagePtr msg_) override;
    bool computePathGradient(const Vector3f& current_pose);
    void publishPath();
    srrg2_core::Point2iVectorCloud _obstacles;
    // observation_model
    //! computes the likelihood of an observation, given a particle
    //! if endpoint_distances is !=0, it will contain the distance between each endpoint and the closest occupied cell
    double likelihood(const Eigen::Vector3f& pose,
                      bool compute_endpoint_distances=false) const;
    double _last_update_time=0;
    int _num_updates=0;
    bool _is_updated=true;
    mutable bool _cost_map_changed_flag=false;
    mutable Eigen::Vector3f _last_goal=Eigen::Vector3f::Zero();
    Vector3fVector  _path;
  };
}
