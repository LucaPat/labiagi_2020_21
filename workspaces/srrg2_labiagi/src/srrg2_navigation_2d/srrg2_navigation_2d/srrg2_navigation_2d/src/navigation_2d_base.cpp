#include "navigation_2d_base.h"
#include <srrg_data_structures/path_matrix_distance_search.h>
#include <srrg_config/configurable_command.h>

#include <unistd.h>

namespace srrg2_navigation_2d {
  using namespace srrg2_core;
  using namespace std;

  Navigation2DBase::Navigation2DBase(){
    addCommand (new ConfigurableCommand_
                < Navigation2DBase, typeof(&Navigation2DBase::cmdLoadMap), std::string, std::string>
                (this,
                 "loadMap",
                 "loads a map from a yaml file",
                 &Navigation2DBase::cmdLoadMap));
  }

  bool Navigation2DBase::cmdLoadMap(std::string& response, const std::string&filename) {
    response = className() + "| loading map from  file ["+filename+"]";
    return loadMap(filename);
  }
  
  bool Navigation2DBase::loadMap(const std::string& filename) {
    const int line_size_max=1024;
    ifstream is(filename.c_str());
    if (! is) {
      return false;
    }
    float resolution=0;
    float occupied_thresh=0;
    float free_thresh=0;
    std::string image_file;
    bool negate=0;
    while (is) {
      char line[line_size_max];
      is.getline(line, line_size_max);
      istringstream ls(line);
      std::string tag;
      ls >> tag;
      if (tag=="resolution:") {
        ls >> resolution;
        continue;
      }
      if (tag=="negate:") {
        ls >> negate;
        continue;
      }
      if (tag=="free_thresh:"){
        ls >> free_thresh;
        continue;
      }
      if (tag=="occupied_thresh:"){
        ls >> occupied_thresh;
        continue;
      }
      if (tag=="origin:") {
        continue;
      }
      if (tag=="image:") {
        ls >>image_file;
        continue;
      }
    }
    if (resolution==0
        || !image_file.length())
      return false;
    cv::Mat cv_img=cv::imread(image_file, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
    ImageUInt8 image;
    image.fromCv(cv_img);
    std::cerr << "Got map" << image.rows() << "x" << image.cols() << std::endl;
    std::cerr << "res: " << resolution << std::endl;
    size_t rows= image.rows();
    size_t cols= image.cols();
    GridMap2DPtr grid_map(new GridMap2D);
    grid_map->setSize(Eigen::Vector2i(rows, cols));
    grid_map->setResolution(resolution);
    using PropertyImageOccupancyType=Property_<ImageOccupancyType>;
    param_occupancy_threshold.setValue(occupied_thresh);
    param_free_threshold.setValue(free_thresh);
    std::cerr << "free threshold: " << param_free_threshold.value() << std::endl;
    std::cerr << "occupied threshold: " << param_occupancy_threshold.value() << std::endl;
    
    PropertyImageOccupancyType* occ_prop=new PropertyImageOccupancyType(param_occupancy_layer.value(),
                                                                        "",
                                                                        grid_map.get(),
                                                                        Matrix_<uint8_t>(),
                                                                        nullptr);
    Matrix_<uint8_t>& occ_map=occ_prop->value();
    occ_map.resize(rows,cols);
    for (size_t r=0; r<rows; ++r)
      for (size_t c=0; c<cols; ++c) {
        occ_map.at(r,c)=image.at(r,c);
      }
    cv::Mat img2;

    // adjust external coords for ROS
    float x_size=rows*resolution;
    float y_size=cols*resolution;
    Eigen::Vector3f origin(y_size/2, x_size/2, -M_PI/2);
    grid_map->setOrigin(geometry2d::v2t(origin));

    setMap(grid_map);
    return true;
  }
  
  void Navigation2DBase::setMap(GridMap2DPtr grid_map_) {
    std::cerr << "setting map " << std::endl;
       
    assert(grid_map_ && "grid map not set");
    _grid_map=grid_map_;
    float occ_threshold=(1.-param_occupancy_threshold.value())*255;
    float free_threshold=(1.-param_free_threshold.value())*255;
    _resolution=_grid_map->resolution();
    _inverse_resolution = 1./_resolution;
    
    using PropertyImageOccupancyType=Property_<ImageOccupancyType>;
    PropertyImageOccupancyType* occ_prop=_grid_map->property<PropertyImageOccupancyType>(param_occupancy_layer.value());
    if (! occ_prop) {
      throw std::runtime_error(std::string("occupancy layer [")
                               +  param_occupancy_layer.value() +
                               "] not found in map");
    }
    _map=occ_prop->value();
    _obstacles.clear();
    const size_t rows=_map.rows();
    const size_t cols=_map.cols();

    std::cerr << "setting map " << rows << " x " << cols << std::endl;

    //DEBUG
    std::cerr << "occ_th: " << occ_threshold << " free_th:" << free_threshold << std::endl;
    _img_occ.create(rows,cols, CV_8UC1);
    int free_count = 0, occ_count = 0, unknown_count = 0;
    for (size_t r = 0; r < rows; ++r){
      for (size_t c = 0; c < cols; ++c) {
        unsigned char& src=_map.at(r,c);
        if (src<occ_threshold) {
          occ_count++;
          Point2i p;
          p.coordinates()=Eigen::Vector2i(r,c);
          _obstacles.push_back(p);
          src=0;
        } else if (src>free_threshold) {
          free_count++;
          src=255;
        } else {
          unknown_count++;
          src=127;
        }
        _img_occ.at<uint8_t>(r,c)=src;
      }
    }
    //std::cerr << "writing map" << std::endl;
    //cv::imwrite("proc_occ.pgm", _img_occ);
    cerr << "free: " << free_count << endl;
    cerr << "unknown: " << unknown_count << endl;
    // cerr << "occupied: " << occ_count << endl;
    _distance_map.resize(rows,cols);
    
    PathMatrixDistanceSearch dmap_calculator;
    dmap_calculator.setPathMatrix(&_distance_map);
    float dmax_in_pixels=param_max_point_distance.value()*_inverse_resolution;
    dmap_calculator.param_max_distance_squared_pxl.setValue(dmax_in_pixels *  dmax_in_pixels);
    dmap_calculator.setGoals(_obstacles);
    dmap_calculator.compute();

    Matrix_<float>* distances=nullptr;
    if (param_distance_layer.value().length()) {
      using PropertyDistanceType=Property_<Matrix_<float> >;
      PropertyDistanceType* dist_prop=_grid_map->property<PropertyDistanceType>(param_distance_layer.value());
      if (! dist_prop) {
        dist_prop=new PropertyDistanceType(param_distance_layer.value(),
                                           "",
                                           _grid_map.get(),
                                           Matrix_<float>(),
                                           nullptr);
      }
      distances= &dist_prop->value();
      distances->resize(rows,cols);
    }

    int k=0;
    _free_cells.resize(rows*cols);
    
    _img_dist.create(rows,cols, CV_8UC1);
    for (size_t r=0; r<rows; ++r) {
      for (size_t c=0; c<cols; ++c) {
        PathMatrixCell& cell=_distance_map.at(r,c);
        unsigned char pixel = _map.at(r,c);
        cell.distance=sqrt(cell.distance)*_resolution;
         if (pixel==127) {
          cell.distance=-cell.distance; // unknown
        }
        _img_dist.at<uint8_t>(r,c)=255*(cell.distance/param_max_point_distance.value());
        if (distances) {
          distances->at(r,c)=cell.distance;
        }
        if (pixel==255 && cell.distance > param_robot_radius.value()) {
          _free_cells[k]=grid2world(r,c);
          ++k;
        }
      }
    }
    //cv::imwrite("proc_dist.pgm", _img_dist);
    _free_cells.resize(k);
    cerr << "valid: " << _free_cells.size() << endl;
    _map_changed_flag=false;
  }

  void Navigation2DBase::handleMapChanged(){
    if (!_map_changed_flag)
      return;
    setMap(this->_grid_map);
    _map_changed_flag=false;
  }
}

