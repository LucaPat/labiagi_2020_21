#include "planner_2d.h"
#include <cmath>
#include <iostream>
#include <srrg_config/configurable_command.h>
#include <srrg_data_structures/path_matrix_dijkstra_search.h>
#include <srrg_data_structures/path_matrix_distance_search.h>
#include <srrg_geometry/geometry2d.h>
#include <srrg_messages/messages/laser_message.h>
#include <srrg_messages/messages/odometry_message.h>
#include <srrg_messages/messages/path_message.h>
#include <srrg_messages/messages/pose_array_message.h>
#include <srrg_messages/messages/pose_stamped_message.h>
#include <srrg_messages/messages/pose_with_covariance_stamped_message.h>
#include <srrg_messages/messages/transform_events_message.h>
#include <srrg_pcl/point.h>
#include <unistd.h>

namespace srrg2_navigation_2d {
using namespace srrg2_core;
using namespace std;

Planner2D::Planner2D() {

  addCommand(new ConfigurableCommand_<Planner2D, typeof(&Planner2D::cmdSetGoal),
                                      std::string, float, float, float>(
      this, "setPose", "sets the pose x y theta", &Planner2D::cmdSetGoal));
}

bool Planner2D::cmdSetGoal(std::string &response, float x, float y,
                           float theta) {
  response = "";
  bool result = computePolicy(Vector3f(x, y, theta));
  return result;
}

bool Planner2D::handleSetGoal(BaseSensorMessagePtr msg_) {
  if (msg_->topic.value() != param_goal_pose_topic.value())
    return false;
  std::cerr << "received goal" << std::endl;
  PoseStampedMessagePtr set_goal_msg =
      std::dynamic_pointer_cast<PoseStampedMessage>(msg_);
  if (!set_goal_msg) {
    std::cerr << "cast error in setting the goal" << std::endl;
    return false;
  }
  Isometry2f iso = geometry3d::get2dFrom3dPose(
      geometry3d::v2t(set_goal_msg->pose.value().pose_vector.value()));
  Vector3f goal = geometry2d::t2v(iso);
  std::cerr << "Goal: " << goal.transpose() << std::endl;
  bool goal_ok = computePolicy(goal);
  std::cerr << "goal_ok:" << goal_ok << std::endl;
  return true;
}

void Planner2D::setMap(GridMap2DPtr grid_map) {
  Navigation2DBase::setMap(grid_map);
  if (!_grid_map)
    return;

  std::cerr << "creating cost layers" << std::endl;
  Vector2i sizes = _grid_map->GridMap2DHeader::size();
  int rows = sizes.x();
  int cols = sizes.y();

  // installs 3 new layers
  PropertyCostType *prop_cost =
      _grid_map->property<PropertyCostType>("cost_map");
  if (!prop_cost) {
    prop_cost = new PropertyCostType("cost_map", "", _grid_map.get(),
                                     Matrix_<float>(rows, cols), nullptr);
  }
  prop_cost->value().fill(-1.f);

  PropertyCostType *prop_cost_dx =
      _grid_map->property<PropertyCostType>("cost_map_dx");
  if (!prop_cost_dx) {
    prop_cost_dx = new PropertyCostType("cost_map_dx", "", _grid_map.get(),
                                        Matrix_<float>(rows, cols), nullptr);
  }
  prop_cost_dx->value().fill(0.f);

  PropertyCostType *prop_cost_dy =
      _grid_map->property<PropertyCostType>("cost_map_dy");
  if (!prop_cost_dy) {
    prop_cost_dy = new PropertyCostType("cost_map_dy", "", _grid_map.get(),
                                        Matrix_<float>(rows, cols), nullptr);
  }
  prop_cost_dy->value().fill(0.f);
}

void Planner2D::updateCostLayers() const {
  Matrix_<float> &cost_map =
      _grid_map->property<PropertyCostType>("cost_map")->value();
  Matrix_<float> &cost_map_dx =
      _grid_map->property<PropertyCostType>("cost_map_dx")->value();
  Matrix_<float> &cost_map_dy =
      _grid_map->property<PropertyCostType>("cost_map_dy")->value();
  cost_map.fill(-1);
  cost_map_dx.fill(0);
  cost_map_dy.fill(0);
  const size_t rows = cost_map.rows();
  const size_t cols = cost_map.cols();
  assert(rows == _distance_map.rows() && "row size mistmatch");
  assert(cols == _distance_map.cols() && "cols size mistmatch");

  // fill cost layer
  for (size_t r = 0; r < rows; ++r) {
    for (size_t c = 0; c < cols; ++c) {
      const PathMatrixCell &cell = _distance_map.at(r, c);
      float &cost = cost_map.at(r, c);
      if (cell.parent)
        cost = cell.cost;
    }
  }

  // fill derivatives
  for (size_t r = 1; r < rows - 1; ++r) {
    for (size_t c = 1; c < cols - 1; ++c) {
      const float &cc = cost_map.at(r, c);
      const float &cx = cost_map.at(r + 1, c);
      const float &cy = cost_map.at(r, c + 1);
      if (cc < 0 || cx < 0 || cy < 0)
        continue;
      cost_map_dx.at(r, c) = cx - cc;
      cost_map_dy.at(r, c) = cy - cc;
    }
  }
}

bool Planner2D::computePathGradient(const Vector3f &current_pose) {
  if (!_grid_map)
    return false;
  _path.clear();
  Matrix_<float> &cost_map =
      _grid_map->property<PropertyCostType>("cost_map")->value();
  Matrix_<float> &cost_map_dx =
      _grid_map->property<PropertyCostType>("cost_map_dx")->value();
  Matrix_<float> &cost_map_dy =
      _grid_map->property<PropertyCostType>("cost_map_dy")->value();
  Vector2f indices_float =
      _grid_map->global2floatIndices(current_pose.head<2>());
  int max_steps = 100000;
  int steps = 0;
  std::cerr << "start: " << current_pose.transpose() << std::endl;
  std::cerr << "goal: " << _last_goal.transpose() << std::endl;
  bool goal_ok = false;
  while (steps < max_steps) {
    Vector2i indices = indices_float.cast<int>();
    // outside, stop
    if (!cost_map.inside(indices)) {
      cerr << "outside" << endl;
      return false;
    }
    // invalid stop
    if (cost_map.at(indices) < 0) {
      cerr << "cost sucks" << endl;
      return false;
    }
    // no gradient, stop
    if (cost_map_dx.at(indices) == 0 && cost_map_dy.at(indices) == 0) {
      cerr << "no gradient" << endl;
      break;
    }
    // get interpolated gradient
    float cost_interpolated;
    Vector2f gradient;
    bool interpolation_ok =
        cost_map_dx.getSubPixel(gradient.x(), indices_float) &&
        cost_map_dy.getSubPixel(gradient.y(), indices_float) &&
        cost_map.getSubPixel(cost_interpolated, indices_float);

    // no interpolation possible, we stop
    if (!interpolation_ok) {
      cerr << "interpolation fail" << endl;
      break;
    }

    if (gradient.norm() < 1) {
      // cerr << "idx: [" << indices_float.transpose() << "] "
      //      << "ig: [" << gradient.transpose() << "] "
      //      << "ic: " << cost_interpolated << " "
      //      << "c: " << cost_map.at(indices) << " "
      //      << "g: [" << cost_map_dx.at(indices) << " "
      //      <<  cost_map_dy.at(indices) << "]" << endl;

      break;
    }
    // cerr << "(c: " << cost_interpolated<< ")";
    gradient.normalize();
    Vector3f trj_pose;
    trj_pose.head<2>() = _grid_map->floatIndices2global(indices_float);
    trj_pose(2) = atan2(trj_pose.y(), trj_pose.x());
    _path.push_back(trj_pose);
    indices_float -= gradient;
    ++steps;
    if ((trj_pose.head<2>() - _last_goal.head<2>()).squaredNorm() <
        2 * _resolution) {
      goal_ok = true;
      break;
    }
  }
  return goal_ok;
}

bool Planner2D::handleScan(BaseSensorMessagePtr msg_) {
  LaserMessagePtr scan = std::dynamic_pointer_cast<LaserMessage>(msg_);
  if (!scan) {
    return false;
  }
  _last_update_time = scan->timestamp.value();
  return true;
}

bool Planner2D::computePolicy(const Vector3f &goal) const {
  if (!_grid_map)
    return false;
  Vector2i goal_int = world2grid(goal.head<2>());
  if (!_distance_map.inside(goal_int)) {
    std::cerr << "goal out of map" << std::endl;
    return false;
  }
  PathMatrixCell &goal_cell = _distance_map.at(goal_int);
  if (goal_cell.distance < param_robot_radius.value()) {
    std::cerr << "goal too close or inside obstacles" << std::endl;
  }

  std::cerr << "computing policy... ";
  PathMatrixDijkstraSearch path_search;
  path_search.setPathMatrix(&_distance_map);
  std::vector<float> cost_poly;
  path_search.param_min_distance.setValue(param_robot_radius.value());
  cost_poly.push_back(_resolution);
  cost_poly.push_back(10);

  path_search.param_cost_polynomial.setValue(cost_poly);
  Point2iVectorCloud goals;
  Point2i goal_pt;
  goal_pt.coordinates() = goal_int;
  goals.push_back(goal_pt);
  _last_goal = goal;

  std::cerr << "resetting search... ";
  path_search.reset();
  std::cerr << "ok" << std::endl;

  std::cerr << "setting goal... ";
  std::cerr << goal.transpose() << std::endl;
  int num_good_goals = path_search.setGoals(goals);
  if (!num_good_goals) {
    std::cerr << "fail" << std::endl;
    return false;
  } else {
    std::cerr << "ok" << std::endl;
  }

  std::cerr << "computing ... ";
  path_search.compute();
  std::cerr << "Done" << std::endl;

  std::cerr << "updating cost layers ... ";
  updateCostLayers();
  std::cerr << "Done" << std::endl;

  return true;
}

void Planner2D::publishPath() {
  static int count = 0;
  if (!_grid_map)
    return;
  if (!platform()) {
    return;
  }

  Isometry2f robot_pose;

  bool tf_result =
      platform()->getTransform(robot_pose, param_base_link_frame_id.value(),
                               param_map_frame_id.value(), _last_update_time);
  if (!tf_result) {
    return;
  }

  Vector3f mean = geometry2d::t2v(robot_pose);

  PathMessagePtr path(new PathMessage);
  path->topic.setValue("/path");
  path->frame_id.setValue("/map");
  path->timestamp.setValue(_last_update_time);
  path->seq.setValue(++count);

  bool gradient_ok = computePathGradient(mean);
  if (!gradient_ok) {
    std::cerr << "no path found to goal" << std::endl;
    propagateMessage(path);
    return;
  }
  std::cerr << "gradient path ok, size:" << _path.size() << std::endl;

  for (size_t i = 0; i < _path.size(); ++i) {
    PoseStampedMessage pose;
    pose.topic.setValue("/path");
    pose.frame_id.setValue("/map");
    pose.timestamp.setValue(_last_update_time);
    pose.seq.setValue(i);
    pose.pose.value().setPose(_path[i]);
    path->poses.pushBack(pose);
  }
  propagateMessage(path);
}

bool Planner2D::putMessage(BaseSensorMessagePtr msg_) {

  if (!_grid_map) {
    std::cerr << className() << " ptr:" << this << " waiting for map"
              << std::endl;
    return false;
  }
  if (!platform()) {
    std::cerr << className() << " ptr:" << this << " waiting for tf tree"
              << std::endl;
  }
  _is_updated = false;
  handleMapChanged();
  if (handleSetGoal(msg_)) {
    publishPath();
    return true;
  }
  if (!handleScan(msg_)) {
    return false;
  }

  return true;
}

} // namespace srrg2_navigation_2d
