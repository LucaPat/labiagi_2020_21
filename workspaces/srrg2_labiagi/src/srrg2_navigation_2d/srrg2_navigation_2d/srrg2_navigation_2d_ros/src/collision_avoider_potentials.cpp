#include "Eigen/Geometry"
#include "srrg_pcl/point_types.h"
#include "tf_helpers.h"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <srrg2_navigation_2d_msgs/CollisionAvoiderStatus.h>
#include <srrg_data_structures/path_matrix_distance_search.h>
#include <srrg_pcl/normal_computator.h>
#include <srrg_qgl_viewport/viewer_core_shared_qgl.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_viewer/viewer_manager_shared.h>
#include <stdexcept>
#include <thread>

using namespace srrg2_core;

// working globals
ros::Publisher cmd_vel_publisher;
ros::Publisher status_publisher;
std::shared_ptr<tf::TransformListener> listener;
Point2fVectorCloud last_scan_points;
Point2fVectorCloud scan_points_all;
double last_scan_stamp = 0;

// hardcoded parameters
// float action_force_range         = 0.5;
std::string cmd_vel_input_topic  = "/cmd_vel_input";
std::string cmd_vel_output_topic = "/cmd_vel";
std::string scan_topic           = "/base_scan";
std::string status_topic         = "/collision_avoider_status";
std::string base_link_frame_id   = "/base_link";
float robot_radius               = 0.8 /*0.2*/;
float voxelize_res               = 0.05;
float angular_loss               = 10;
float max_angular_correction     = 0.5;
bool verbose                     = false;
float linear_velocity_min        = 1e-2;
Vector2f offset(100, 100);
PointNormal2f parent_in_world;

srrg2_navigation_2d_msgs::CollisionAvoiderStatus status_msg;
static const std::string canvas_name("my_canvas");

Point2iVectorCloud toPixel(const PointNormal2fVectorCloud& point_cloud_) {
  Point2iVectorCloud pixel_vector_cloud;
  pixel_vector_cloud.resize(point_cloud_.size());
  Point2i point;
  for (size_t i = 0; i < point_cloud_.size(); i++) {
    float current_point_x   = std::floor(point_cloud_[i].coordinates().x() / voxelize_res);
    float current_point_y   = std::floor(point_cloud_[i].coordinates().y() / voxelize_res);
    point.coordinates().x() = (int) current_point_x + offset.x();
    point.coordinates().y() = (int) -current_point_y + offset.y();
    pixel_vector_cloud[i]   = point;
  }
  return pixel_vector_cloud;
}

PointNormal2fVectorCloud computeNormal(const Point2fVectorCloud& point_cloud_) {
  if (point_cloud_.size() == 0) {
    std::string msg = "Point cloud is empty!";
    throw std::runtime_error(msg);
  }
  PointNormal2fVectorCloud point_normal_cloud;
  NormalComputator1DSlidingWindowNormal normal_computator;
  PointNormal2fVectorCloud current_vector_cloud;
  float voxelize_res = 0.8;
  float squared_res  = voxelize_res * voxelize_res;
  Vector2f difference(0.f, 0.f);
  PointNormal2f normal_point;
  normal_point.coordinates() = point_cloud_[0].coordinates();
  current_vector_cloud.emplace_back(normal_point);
  for (size_t i = 1; i < point_cloud_.size(); ++i) {
    difference = point_cloud_[i].coordinates() - point_cloud_[i - 1].coordinates();
    if (difference.squaredNorm() < squared_res) {
      // LOG(point_cloud_[i].coordinates());
      normal_point.coordinates() = point_cloud_[i].coordinates();
      current_vector_cloud.emplace_back(normal_point);
    } else {
      normal_computator.computeNormals(current_vector_cloud);
      point_normal_cloud.insert(
        point_normal_cloud.end(), current_vector_cloud.begin(), current_vector_cloud.end());
      current_vector_cloud.clear();
    }
  }
  normal_computator.computeNormals(current_vector_cloud);
  point_normal_cloud.insert(
    point_normal_cloud.end(), current_vector_cloud.begin(), current_vector_cloud.end());
  current_vector_cloud.clear();
  return point_normal_cloud;
}

Vector2f computeNextDirection(const PathMatrix& distance_map_, PathMatrixCell* parent_cell_) {
  // bb compute gradient
  Vector2f gradient(0.f, 0.f);
  const int* neighbor_offsets = distance_map_.eightNeighborOffsets();
  PathMatrixCell* down        = parent_cell_ + neighbor_offsets[1];
  PathMatrixCell* up          = parent_cell_ + neighbor_offsets[6];
  PathMatrixCell* left        = parent_cell_ + neighbor_offsets[3];
  PathMatrixCell* right       = parent_cell_ + neighbor_offsets[4];
  gradient.x()                = right->distance - left->distance;
  gradient.y()                = down->distance - up->distance;
  return gradient.normalized();
}

bool scan2points(const sensor_msgs::LaserScan& scan) {
  Isometry2f laser_in_robot;
  if (!getTfTransform(
        laser_in_robot, *listener, base_link_frame_id, scan.header.frame_id, ros::Time(0))) {
    return false;
  }
  float crop_range = scan.range_max;
  // static Point2fVectorCloud scan_points_all;
  scan_points_all.reserve(scan.ranges.size());
  scan_points_all.clear();
  float alpha = scan.angle_min;
  for (size_t i = 0; i < scan.ranges.size(); ++i, alpha += scan.angle_increment) {
    float r = scan.ranges[i];
    if (r < scan.range_min)
      continue;
    if (r > crop_range)
      continue;
    float c = cos(alpha), s = sin(alpha);
    Point2f p;
    p.coordinates().x() = r * c;
    p.coordinates().y() = r * s;
    scan_points_all.push_back(p);
  }
  scan_points_all.transformInPlace(laser_in_robot);

  return true;
}

void updateVelocity(geometry_msgs::Twist& twist_output_, const PointNormal2f& parent_in_world_) {
  Vector2f normal = parent_in_world_.normal().normalized();
  Vector2f twist_output_vector(0.f, 0.f);
  twist_output_vector << twist_output_.linear.x, twist_output_.linear.y;
  twist_output_vector -= twist_output_vector.dot(normal) * normal;
  twist_output_.linear.x = twist_output_vector.x();
  twist_output_.linear.y = twist_output_vector.y();
}

void cmdVelCallback(const geometry_msgs::Twist& twist_input) {
  status_msg.header.stamp  = ros::Time::now();
  status_msg.cmd_vel_input = twist_input;
  if (!scan_points_all.size() || twist_input.linear.x < 0) {
    cmd_vel_publisher.publish(twist_input);
    status_msg.cmd_vel_output = twist_input;
    status_msg.status         = "clear";
    status_publisher.publish(status_msg);
    return;
  }
  // compute the twist variation
  PointNormal2fVectorCloud scan_points_all_derivative = computeNormal(scan_points_all);
  // Vector2f voxelize_coeffs(voxelize_res, voxelize_res);
  // last_scan_points.clear();
  // scan_points_all.voxelize(std::back_insert_iterator<Point2fVectorCloud>(last_scan_points),
  //                          voxelize_coeffs);
  // toPixel(last_scan_points);
  Point2iVectorCloud pixel_point_cloud = toPixel(scan_points_all_derivative);
  int rows                             = offset.y() * 2;
  int cols                             = offset.x() * 2;
  float robot_radius_pixel             = robot_radius / voxelize_res;
  PathMatrix distance_map(rows, cols);
  PathMatrixDistanceSearch dmap_calculator;
  dmap_calculator.setPathMatrix(&distance_map);
  dmap_calculator.param_max_distance_squared_pxl.setValue(robot_radius_pixel * robot_radius_pixel);
  dmap_calculator.setGoals(pixel_point_cloud);
  dmap_calculator.compute();
  auto robot_cell_parent            = distance_map(offset.y(), offset.x()).parent;
  geometry_msgs::Twist twist_output = twist_input;
  if (!robot_cell_parent) {
    // bb No obstacles in the nearby of the robot: parent is nullptr!
    cmd_vel_publisher.publish(twist_input);
    status_msg.cmd_vel_output = twist_input;
    status_msg.status         = "clear";
    status_publisher.publish(status_msg);
    return;
  }
  Vector2f gradient = computeNextDirection(distance_map, robot_cell_parent);
  std::cerr << "gradient" << gradient << std::endl;
  Vector2i parent_in_pixel = distance_map.pos(robot_cell_parent);

  auto it = std::find_if(pixel_point_cloud.begin(),
                         pixel_point_cloud.end(),
                         [parent_in_pixel](const auto& point_in_pixel) {
                           bool x_coordinates_equal =
                             point_in_pixel.coordinates().x() == parent_in_pixel.x();
                           bool y_coordinates_equal =
                             point_in_pixel.coordinates().y() == parent_in_pixel.y();
                           return x_coordinates_equal && y_coordinates_equal;
                         });
  if (it == pixel_point_cloud.end()) {
    throw std::runtime_error("Parent not found when expected!!!");
  }
  auto ptr_diff   = std::distance(pixel_point_cloud.begin(), it);
  auto ptr        = scan_points_all_derivative.begin() + ptr_diff;
  parent_in_world = *ptr;
  updateVelocity(twist_output, parent_in_world);

  status_msg.cmd_vel_output = twist_output;
  cmd_vel_publisher.publish(twist_output);
  status_publisher.publish(status_msg);
}

void producer(ViewerCanvasPtr canvas_, const Point2fVectorCloud& other_points_);

void scanCallback(const sensor_msgs::LaserScan& scan) {
  if (!scan2points(scan))
    return;
  last_scan_stamp = scan.header.stamp.toSec();
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "trajectory_follower");
  ros::NodeHandle nh;
  nh.getParam("cmd_vel_input_topic", cmd_vel_input_topic);
  nh.getParam("cmd_vel_output_topic", cmd_vel_output_topic);
  nh.getParam("scan_topic", scan_topic);
  nh.getParam("base_link_frame_id", base_link_frame_id);
  nh.getParam("robot_radius", robot_radius);
  nh.getParam("voxelize_res", voxelize_res);
  nh.getParam("angular_loss", angular_loss);
  nh.getParam("max_angular_correction", max_angular_correction);
  nh.getParam("verbose", verbose);
  std::cerr << argv[0] << ": running with params" << std::endl;

  std::cerr << "_cmd_vel_input_topic:=" << cmd_vel_input_topic << std::endl;
  std::cerr << "_cmd_vel_output_topic:=" << cmd_vel_output_topic << std::endl;
  std::cerr << "_scan_topic:=" << scan_topic << std::endl;
  std::cerr << "_base_link_frame_id:=" << base_link_frame_id << std::endl;
  std::cerr << "_status_topic:=" << status_topic << std::endl;
  std::cerr << "_robot_radius:=" << robot_radius << std::endl;
  std::cerr << "_voxelize_res:=" << voxelize_res << std::endl;
  std::cerr << "_angular_loss:=" << angular_loss << std::endl;
  std::cerr << "_max_angular_correction:=" << max_angular_correction << std::endl;
  std::cerr << "verbose:=" << verbose << std::endl;

  status_publisher =
    nh.advertise<srrg2_navigation_2d_msgs::CollisionAvoiderStatus>(status_topic, 10);
  cmd_vel_publisher          = nh.advertise<geometry_msgs::Twist>(cmd_vel_output_topic, 10);
  status_msg.header.frame_id = base_link_frame_id;
  ros::Subscriber cmd_vel_input_subscriber = nh.subscribe(cmd_vel_input_topic, 10, cmdVelCallback);
  ros::Subscriber scan_subscriber          = nh.subscribe("base_scan", 10, scanCallback);
  ros::Rate loop_rate(50);
  listener.reset(new tf::TransformListener);
  QApplication qapp(argc, argv);
  srrg2_qgl_viewport::ViewerCoreSharedQGL viewer_core(argc, argv, &qapp);
  const ViewerCanvasPtr& canvas = viewer_core.getCanvas(canvas_name);
  srrg2_qgl_viewport::ViewerCoreSharedQGL::stop();

  viewer_core.startViewerServer();
  std::thread producer_t;
  while (ros::ok()) {
    ros::spinOnce();
    loop_rate.sleep();
    producer_t = std::thread(producer, canvas, scan_points_all);
  }
  producer_t.join();
}

void node(ViewerCanvasPtr canvas_, char** argv_) {
  ros::NodeHandle nh;
  nh.getParam("cmd_vel_input_topic", cmd_vel_input_topic);
  nh.getParam("cmd_vel_output_topic", cmd_vel_output_topic);
  nh.getParam("scan_topic", scan_topic);
  nh.getParam("base_link_frame_id", base_link_frame_id);
  nh.getParam("robot_radius", robot_radius);
  nh.getParam("voxelize_res", voxelize_res);
  nh.getParam("angular_loss", angular_loss);
  nh.getParam("max_angular_correction", max_angular_correction);
  nh.getParam("verbose", verbose);
  std::cerr << argv_[0] << ": running with params" << std::endl;

  std::cerr << "_cmd_vel_input_topic:=" << cmd_vel_input_topic << std::endl;
  std::cerr << "_cmd_vel_output_topic:=" << cmd_vel_output_topic << std::endl;
  std::cerr << "_scan_topic:=" << scan_topic << std::endl;
  std::cerr << "_base_link_frame_id:=" << base_link_frame_id << std::endl;
  std::cerr << "_status_topic:=" << status_topic << std::endl;
  std::cerr << "_robot_radius:=" << robot_radius << std::endl;
  std::cerr << "_voxelize_res:=" << voxelize_res << std::endl;
  std::cerr << "_angular_loss:=" << angular_loss << std::endl;
  std::cerr << "_max_angular_correction:=" << max_angular_correction << std::endl;
  std::cerr << "verbose:=" << verbose << std::endl;

  status_publisher =
    nh.advertise<srrg2_navigation_2d_msgs::CollisionAvoiderStatus>(status_topic, 10);
  cmd_vel_publisher          = nh.advertise<geometry_msgs::Twist>(cmd_vel_output_topic, 10);
  status_msg.header.frame_id = base_link_frame_id;
  ros::Subscriber cmd_vel_input_subscriber = nh.subscribe(cmd_vel_input_topic, 10, cmdVelCallback);
  ros::Subscriber scan_subscriber          = nh.subscribe("base_scan", 10, scanCallback);
  ros::Rate loop_rate(50);
  listener.reset(new tf::TransformListener);
  while (ros::ok()) {
    ros::spinOnce();
    loop_rate.sleep();
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "trajectory_follower");
  QApplication qapp(argc, argv);
  srrg2_qgl_viewport::ViewerCoreSharedQGL viewer_core(argc, argv, &qapp);
  const ViewerCanvasPtr& canvas = viewer_core.getCanvas(canvas_name);
  srrg2_qgl_viewport::ViewerCoreSharedQGL::stop();

  viewer_core.startViewerServer();
  std::thread node_t(node, canvas, argv);
  node_t.join();
  return 0;
}

void producer(ViewerCanvasPtr canvas_, const Point2fVectorCloud& other_points_) {
  std::string text = "testing viewer manager";
  while (!srrg2_qgl_viewport::ViewerCoreSharedQGL::isRunning()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  // ia lettse draw (*mario style*)
  uint64_t frame_counter = 0;
  double duration        = 0.0;
  while (srrg2_qgl_viewport::ViewerCoreSharedQGL::isRunning()) {
    ++frame_counter;
    SystemUsageCounter::tic();
    // ia draw a colored points polygon
    canvas_->pushPointSize();
    canvas_->setPointSize(1.5);
    canvas_->putPoints(other_points_);
    canvas_->popAttribute();

    canvas_->flush();
    duration += SystemUsageCounter::toc();

    if (duration >= 2.0) {
      std::cerr << "producer FPS = " << FG_GREEN((double) frame_counter / duration) << " Hz\r";
      std::flush(std::cerr);
      duration      = 0.0;
      frame_counter = 0;
    }
    //    std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));
  }
}
