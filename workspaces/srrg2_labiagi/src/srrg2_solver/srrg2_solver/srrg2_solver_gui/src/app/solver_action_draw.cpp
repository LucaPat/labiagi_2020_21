#include "solver_action_draw.h"
#include <srrg_solver/solver_core/solver.h>
namespace srrg2_solver_gui {
  using namespace srrg2_solver;
  

  SolverActionDraw::SolverActionDraw() {
    param_event.setValue(Solver::SolverEvent::ComputeEnd);
  }

  SolverActionDraw::~SolverActionDraw() {}


  void SolverActionDraw::doAction() {
    if (_canvas == nullptr) {
      throw std::runtime_error("SolverActionDraw::doAction|invalid canvas");
    }
    if (! _solver_ptr){
      throw std::runtime_error("SolverActionDraw::doAction|invalid solver");
    }
    srrg2_solver::FactorGraphInterface& graph=_solver_ptr->graph();

    for (const auto& v_tuple : graph.variables()) {
      v_tuple.second->_drawImpl(_canvas);
    }

    for (const auto& f : graph.factors()) {
      f.second->_drawImpl(_canvas);
    }

    _canvas->flush();
  }
} // namespace srrg2_solver_gui
