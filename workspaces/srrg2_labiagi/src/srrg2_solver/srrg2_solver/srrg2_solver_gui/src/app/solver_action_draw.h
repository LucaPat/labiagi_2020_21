#pragma once
#include <srrg_solver/solver_core/factor_graph.h>
#include <srrg_solver/solver_core/solver_action_base.h>
#include <srrg_solver/solver_core/solver.h>
#include <srrg_viewer/viewer_canvas.h>

namespace srrg2_solver_gui {

  //! @brief action that draws a factor graph on the canvas
  class SolverActionDraw : public srrg2_solver::SolverActionBase {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    using BaseType = srrg2_solver::SolverActionBase;
    SolverActionDraw();
    virtual ~SolverActionDraw();

    void doAction();
    void setCanvas(srrg2_core::ViewerCanvasPtr canvas_) {_canvas=canvas_;}
  protected:
    srrg2_core::ViewerCanvasPtr _canvas = nullptr;
  };

  using SolverActionDrawPtr = std::shared_ptr<SolverActionDraw>;
} // namespace srrg2_solver_gui
